<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Employee;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

$factory->define(Employee::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$Rzo2ZBLVMXctgi3h2lG/q.NsbC.2JAIhsk3ryWuMJd2zwPgpBTfOu',
        'employment_start_date' => $faker->date(),
        'employment_end_date' => Carbon::parse($faker->date())->lessThanOrEqualTo(Carbon::now()) ? $faker->date() : null ,
        'manager_id' => null,
    ];
});
