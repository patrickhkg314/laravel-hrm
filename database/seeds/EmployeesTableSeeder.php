<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\Hash;
use App\Models\Employee;

class EmployeesTableSeeder extends Seeder
{
    private $sampleData= [
        [
            'email' => 'leandro@acme.com',
            'name' => 'Leandro Luongo',
            'employment_start_date' => '2019-02-07',
            'password' => '$2y$10$WyRn2Rnk2v78mcvxaAEiluEHX.f5X4Tj52msMmqhf3WvvHtxdp4y.',// 12341234
            'manager' => null
        ],
        [
            'email' => 'macie@acme.com',
            'name' => 'Macie Mcafee',
            'employment_start_date' => '2018-06-01',
            'password' => '$2y$10$WyRn2Rnk2v78mcvxaAEiluEHX.f5X4Tj52msMmqhf3WvvHtxdp4y.', // 12341234
            'manager' => 'samuel@acme.com'
        ],
        [
            'email' => 'keven@acme.com',
            'name' => 'Keven Korman',
            'employment_start_date' => '2015-11-10',
            'password' => '$2y$10$WyRn2Rnk2v78mcvxaAEiluEHX.f5X4Tj52msMmqhf3WvvHtxdp4y.', // 12341234
            'manager' => null
        ],
        [
            'email' => 'samuel@acme.com',
            'name' => 'Samuel Stabile',
            'employment_start_date' => '2013-10-07',
            'password' => '$2y$10$WyRn2Rnk2v78mcvxaAEiluEHX.f5X4Tj52msMmqhf3WvvHtxdp4y.', // 12341234
            'manager' => null
        ],
        [
            'email' => 'marisha@acme.com',
            'name' => 'Marisha Mcclary',
            'employment_start_date' => '2017-05-04',
            'password' => '$2y$10$WyRn2Rnk2v78mcvxaAEiluEHX.f5X4Tj52msMmqhf3WvvHtxdp4y.', // 12341234
            'manager' => 'keven@acme.com'
        ],
        [
            'email' => 'minnie@acme.com',
            'name' => 'Minnie Mccalley',
            'employment_start_date' => '2019-04-04',
            'password' => '$2y$10$WyRn2Rnk2v78mcvxaAEiluEHX.f5X4Tj52msMmqhf3WvvHtxdp4y.', // 12341234
            'manager' => 'keven@acme.com'
        ]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $copy = $this->sampleData;
        foreach ($copy as $key => $value) {
            unset($value['manager']);
            Employee::create($value);
        }

        foreach ($this->sampleData as $key => $value) {
            $employee = Employee::where('email',$value['email'])->first();

            if ($value['manager'] == null)
                continue;

            $manager = Employee::where('email',$value['manager'])->first();

            if ($manager != null)
                $employee->manager()->associate($manager)->save();
        }
    }
}
