<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->to('/hrm/home');
});

Route::get('/hrm', function () {
    return redirect()->to('/hrm/home');
});

// Authentication Routes...
Route::get('login', 'HRM\LoginController@showLoginForm')->name('login');
Route::post('login', 'HRM\LoginController@login');
Route::post('logout', 'HRM\LoginController@logout')->name('logout');

// API for Profiles...
Route::post('password','HRM\ProfileController@changePassword');
Route::get('logged-in-employee','HRM\ProfileController@getLoggedInEmployeeData');
Route::get('employee-information','HRM\ProfileController@getEmployeeManagerAndAllSubordinates');

Route::group(['prefix' => '/employees'],function () {
    Route::get('/','HRM\EmployeesController@index');
    Route::get('/active','HRM\EmployeesController@getActiveEmployees');

    Route::get('/new-joiner','HRM\EmployeesController@getNewJoinerEmployees');
    Route::get('/new-joiner/export','HRM\EmployeesController@exportNewJoinerEmployeesReport');

    Route::get('/hierarchy','HRM\EmployeesController@getEmployeeHierarchyData');
    Route::get('/hierarchy/export','HRM\EmployeesController@exportEmployeeHierarchyData');

    Route::post('/','HRM\EmployeesController@add');
    Route::put('/','HRM\EmployeesController@edit');
    Route::delete('/','HRM\EmployeesController@delete');
});

/**
 * To Vue.js. Do not delete this part!!! Otherwise, the Web Interface will fail.
 */
Route::group(['middleware' => 'auth:web'], function() {
    Route::get('/hrm/{path}', function () {
        return view('index');
    })->where('path', '(.*)');
});
