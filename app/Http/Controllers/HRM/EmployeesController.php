<?php

namespace App\Http\Controllers\HRM;

use App\Exports\HierarchyReport;
use App\Http\Controllers\Controller;
use App\Services\ApiService;
use App\Services\EmployeeService;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class EmployeesController extends Controller
{
    protected $employeeService, $apiService;

    /**
     * ProfileController constructor.
     * @param EmployeeService $employeeService
     * @param ApiService $apiService
     */
    public function __construct(EmployeeService $employeeService, ApiService $apiService)
    {
        $this->middleware('auth:web');
        $this->apiService = $apiService;
        $this->employeeService = $employeeService;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $pagination = [
            'descending' => $request->descending == 'true',
            'page' => $request->page,
            'rowsPerPage' => $request->rowsPerPage,
            'sortBy' => $request->sortBy
        ];

        $dataSource = $this->employeeService->getAllEmployees($pagination);

        $data = [
            'data' => $dataSource,
            'total' => $this->employeeService->countAllEmployees(),
        ];

        return $this->apiService->returnSuccess($data);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getActiveEmployees()
    {
        $data = $this->employeeService->getActiveEmployees()->toArray();

        return $this->apiService->returnSuccess($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\ValidatorException
     */
    public function add(Request $request)
    {
        if ($request->employee != null) {
            $data = $request->employee;

            $dataArray = [
                'id' => $this->checkArrayKeyExistAndReturn($data['id']),
                'name' => $this->checkArrayKeyExistAndReturn($data['name']),
                'email' => $this->checkArrayKeyExistAndReturn($data['email']),
                'password' => $this->checkArrayKeyExistAndReturn($data['password']),
                'password_confirmation' => $this->checkArrayKeyExistAndReturn($data['password_confirmation']),
                'employment_start_date' => $this->checkArrayKeyExistAndReturn($data['employment_start_date']),
                'employment_end_date' => $this->checkArrayKeyExistAndReturn($data['employment_end_date']),
            ];
        } else
            $dataArray = [];

        $this->employeeService->addEmployee($dataArray, $request->manager_id);

        return $this->apiService->returnSuccess();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\ValidatorException
     */
    public function edit(Request $request)
    {
        if ($request->employee != null) {
            $data = $request->employee;

            $dataArray = [
                'id' => $this->checkArrayKeyExistAndReturn($data['id']),
                'name' => $this->checkArrayKeyExistAndReturn($data['name']),
                'email' => $this->checkArrayKeyExistAndReturn($data['email']),
                'employment_start_date' => $this->checkArrayKeyExistAndReturn($data['employment_start_date']),
                'employment_end_date' => $this->checkArrayKeyExistAndReturn($data['employment_end_date']),
            ];
        } else
            $dataArray = [];

        $this->employeeService->editEmployee($dataArray, $request->manager_id);

        return $this->apiService->returnSuccess();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\SelfDeleteException
     * @throws \App\Exceptions\ValidatorException
     */
    public function delete(Request $request)
    {
        $this->employeeService->deleteEmployee($request->all());

        return $this->apiService->returnSuccess();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\ValidatorException
     */
    public function getEmployeeHierarchyData(Request $request)
    {
        $data = [
            'employee_data' => $this->employeeService->getSingleEmployees($request->id),
            'all_subordinates' => $this->employeeService->checkEmployeeIndirectSubordinates($request->id)
        ];

        return $this->apiService->returnSuccess($data);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     * @throws \App\Exceptions\ValidatorException
     */
    public function exportEmployeeHierarchyData(Request $request)
    {
        $employee_data = $this->employeeService->getSingleEmployees($request->id);
        $all_subordinates = $this->employeeService->checkEmployeeIndirectSubordinates($request->id);

        return Excel::download(new HierarchyReport($employee_data,$all_subordinates), 'Hierarchy_Report.xlsx');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\ValidatorException
     */
    public function getNewJoinerEmployees(Request $request)
    {
        $output = [
            'data' => $this->employeeService->getNewJoinerEmployeesListInPeriod($request->all())
        ];

        return $this->apiService->returnSuccess($output);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     * @throws \App\Exceptions\ValidatorException
     */
    public function exportNewJoinerEmployeesReport(Request $request)
    {
        return $this->employeeService->exportNewJoinerEmployeesListInPeriod($request->all());
    }

    private function checkArrayKeyExistAndReturn(&$var, $default = null)
    {
        return isset($var) ? $var : $default;
    }
}