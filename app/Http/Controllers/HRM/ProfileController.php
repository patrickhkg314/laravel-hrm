<?php

namespace App\Http\Controllers\HRM;

use App\Http\Controllers\Controller;
use App\Services\ApiService;
use App\Services\EmployeeService;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    protected $employeeService, $apiService;

    /**
     * ProfileController constructor.
     * @param EmployeeService $employeeService
     * @param ApiService $apiService
     */
    public function __construct(EmployeeService $employeeService, ApiService $apiService)
    {
        $this->middleware('auth:web');
        $this->apiService = $apiService;
        $this->employeeService = $employeeService;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLoggedInEmployeeData() {
        return $this->apiService->returnSuccess($this->employeeService->getLoggedInEmployeeData()->toArray());
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\PasswordInvalidException
     * @throws \App\Exceptions\ValidatorException
     */
    public function changePassword(Request $request)
    {
        $this->employeeService->changeEmployeePassword($request->all());

        return $this->apiService->returnSuccess();
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\ValidatorException
     */
    public function getEmployeeManagerAndAllSubordinates()
    {
        $data = [
            'employee_data' => $this->employeeService->getLoggedInEmployeeData(),
            'all_subordinates' => $this->employeeService->checkEmployeeIndirectSubordinates()
        ];

        return $this->apiService->returnSuccess($data);
    }
}