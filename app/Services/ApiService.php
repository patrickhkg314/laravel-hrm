<?php

namespace App\Services;

class ApiService
{

    private $succeedStatus = 200;
    private $failedStatus = 400;

    /**
     * @param array $succeedTextArray
     * @return \Illuminate\Http\JsonResponse
     */
    public function returnSuccess(Array $succeedTextArray = ['message' => 'OK'])
    {
        return response()->json([
            'result_status' => "Success",
            "result_data" => $succeedTextArray
        ], $this->succeedStatus);
    }

    /**
     * @param array $failedTextArray
     * @return \Illuminate\Http\JsonResponse
     */
    public function returnFail(Array $failedTextArray = ['Unknown error'])
    {
        return response()->json([
            'result_status' => "Fail",
            'error_message' => $failedTextArray
        ], $this->failedStatus);
    }
}