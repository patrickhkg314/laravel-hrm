<?php

namespace App\Services;

use App\Exceptions\PasswordInvalidException;
use App\Exceptions\SelfDeleteException;
use App\Exceptions\ValidatorException;
use App\Exports\NewJoinerEmployeesReport;
use App\Repositories\EmployeeRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class EmployeeService
{

    protected $employeeRepository;

    protected $changePasswordValidationRules = [
        'old_password' => "required",
        'new_password' => "required",
        'new_password_confirmation' => "required|same:new_password",
    ];

    protected $newPasswordValidationRules = [
        'password' => "required",
        'password_confirmation' => "required|same:password",
    ];

    protected $employeeExistValidatorRules = [
        'id' => "required|exists:employees,id",
    ];

    protected $employeeValidatorRules = [
        'name' => "required|string",
        'email' => "required|email|unique:employees,email",
        'employment_start_date' => "required|date",
        'employment_end_date' => "nullable|after_or_equal:employment_start_date",
    ];

    protected $filterValidatorRules = [
        'start_date' => "nullable|date",
        'end_date' => "nullable|after_or_equal:start_date",
    ];

    /**
     * EmployeeService constructor.
     * @param EmployeeRepository $employeeRepository
     */
    public function __construct(EmployeeRepository $employeeRepository)
    {
        $this->employeeRepository = $employeeRepository;
    }

    /**
     * @param null $id
     * @return array
     */
    private function employeeValidatorRules($id = null)
    {
        if ($id != null)
            $this->employeeValidatorRules['email'] .= ",$id";

        return $this->employeeValidatorRules;
    }

    /**
     * @return mixed
     */
    public function getLoggedInEmployeeData()
    {
        return $this->employeeRepository->getEmployeeData(Auth::user()->id);
    }

    /**
     * @return mixed
     */
    public function getActiveEmployees()
    {
        return $this->employeeRepository->getActiveEmployeeData();
    }

    /**
     * @param $id
     * @return mixed
     * @throws ValidatorException
     */
    public function getSingleEmployees($id)
    {
        $validator = Validator::make(['id' => $id], $this->employeeExistValidatorRules);

        if ($validator->fails())
            throw new ValidatorException($validator->errors()->toArray());

        return $this->employeeRepository->getEmployeeData($id);
    }

    /**
     * @param array $pagination
     * @return \App\Models\Employee[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAllEmployees(Array $pagination)
    {
        return $this->employeeRepository->getAll($pagination);
    }

    /**
     * @param array $dataArray
     * @return EmployeeRepository
     * @throws ValidatorException
     */
    public function getNewJoinerEmployeesListInPeriod(Array $dataArray)
    {
        $validator = Validator::make($dataArray, $this->filterValidatorRules);

        if ($validator->fails())
            throw new ValidatorException($validator->errors()->toArray());

        if (!array_key_exists('start_date',$dataArray))
            $dataArray['start_date'] = null;
        if (!array_key_exists('end_date',$dataArray))
            $dataArray['end_date'] = null;

        return $this->employeeRepository->getNewJoinerEmployeesListInPeriod($dataArray['start_date'], $dataArray['end_date']);
    }

    /**
     * @param array $dataArray
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     * @throws ValidatorException
     */
    public function exportNewJoinerEmployeesListInPeriod(Array $dataArray)
    {
        $validator = Validator::make($dataArray, $this->filterValidatorRules);

        if ($validator->fails())
            throw new ValidatorException($validator->errors()->toArray());

        if (!array_key_exists('start_date',$dataArray))
            $dataArray['start_date'] = null;
        if (!array_key_exists('end_date',$dataArray))
            $dataArray['end_date'] = null;

        $output =  $this->employeeRepository->getNewJoinerEmployeesListInPeriod($dataArray['start_date'], $dataArray['end_date']);

        return Excel::download(new NewJoinerEmployeesReport($output,$dataArray['start_date'], $dataArray['end_date']), 'New_Joiner_Employees.xlsx');
    }

    /**
     * @return mixed
     */
    public function countAllEmployees()
    {
        return $this->employeeRepository->countAll();
    }

    /**
     * @param array $dataArray
     * @param Int|null $id
     * @throws PasswordInvalidException
     * @throws ValidatorException
     */
    public function changeEmployeePassword(Array $dataArray, Int $id = null)
    {
        if ($id == null)
            $employee = Auth::user();
        else {
            $employee = $this->getSingleEmployees($id);
        }

        $validator = Validator::make($dataArray, $this->changePasswordValidationRules);

        if ($validator->fails())
            throw new ValidatorException($validator->errors()->toArray());

        if (!Hash::check($dataArray['old_password'], $employee->password))
            throw new PasswordInvalidException();

        $this->employeeRepository->updateEmployeePassword($employee->id, $dataArray['new_password']);
    }

    /**
     * @param array $dataArray
     * @throws SelfDeleteException
     * @throws ValidatorException
     */
    public function deleteEmployee(Array $dataArray)
    {
        $employee = Auth::user();

        $validator = Validator::make($dataArray, $this->employeeExistValidatorRules);

        if ($validator->fails())
            throw new ValidatorException($validator->errors()->toArray());

        if ($dataArray['id'] == $employee->id)
            throw new SelfDeleteException();

        $this->employeeRepository->deleteEmployee($dataArray['id']);
    }

    /**
     * @param null $id
     * @return mixed
     * @throws ValidatorException
     */
    public function checkEmployeeIndirectSubordinates($id = null)
    {
        if ($id == null)
            $id = Auth::user()->id;

        $validator = Validator::make(['id' => $id], $this->employeeExistValidatorRules);

        if ($validator->fails()) {
            throw new ValidatorException($validator->errors()->toArray());
        }

        return $this->employeeRepository->checkEmployeeIndirectSubordinates($id);
    }

    /**
     * @param array $dataArray
     * @param null $manager_id
     * @throws ValidatorException
     */
    public function addEmployee(Array $dataArray, $manager_id = null)
    {
        $addRule = array_merge($this->newPasswordValidationRules, $this->employeeValidatorRules());

        $employeeValidator = Validator::make($dataArray, $addRule);

        if ($employeeValidator->fails())
            throw new ValidatorException($employeeValidator->errors()->toArray());

        if ($manager_id != null) {
            $managerValidator = Validator::make(['id' => $manager_id], $this->employeeExistValidatorRules);

            if ($managerValidator->fails())
                throw new ValidatorException($managerValidator->errors()->toArray());
        }

        $this->employeeRepository->addEmployee($dataArray, $manager_id);
    }

    /**
     * @param array $dataArray
     * @param null $manager_id
     * @throws ValidatorException
     */
    public function editEmployee(Array $dataArray, $manager_id = null)
    {
        $id = array_key_exists('id', $dataArray) ? $dataArray['id'] : null;
        $editRule = array_merge($this->employeeExistValidatorRules, $this->employeeValidatorRules($id));

        $employeeValidator = Validator::make($dataArray, $editRule);

        if ($employeeValidator->fails())
            throw new ValidatorException($employeeValidator->errors()->toArray());

        if ($manager_id != null) {
            $managerValidator = Validator::make(['id' => $manager_id], $this->employeeExistValidatorRules);

            if ($managerValidator->fails())
                throw new ValidatorException($managerValidator->errors()->toArray());
        }

        $this->employeeRepository->editEmployee($dataArray, $manager_id);
    }
}
