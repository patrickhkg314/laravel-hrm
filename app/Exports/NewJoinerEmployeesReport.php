<?php

namespace App\Exports;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeSheet;

class NewJoinerEmployeesReport implements FromCollection, ShouldAutoSize, WithEvents
{
    protected $newJoiner, $startDate, $endDate, $count;

    /**
     * NewJoinerEmployeesReport constructor.
     * @param $newJoiner
     * @param $startDate
     * @param $endDate
     */
    public function __construct($newJoiner, $startDate, $endDate)
    {
        $this->newJoiner = $newJoiner;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    /**
     * @return array|\Illuminate\Support\Collection
     */
    public function collection()
    {
        $data = new Collection([
            ['Report Title', 'New Joiner Employee Report'],
            ['Blasting Date', Carbon::now()->toDateTimeString()],
            ['From Date', $this->startDate],
            ['To Date', $this->endDate],
            [''],
            ['Staff ID', 'Name', 'Email', 'Employment Start Date', 'Employment End Date', 'Manager', 'Manager Staff ID'],
        ]);

        foreach ($this->newJoiner as $key => $value) {
            $data->push([
                $value->id,
                $value->name,
                $value->email,
                $value->employment_start_date,
                $value->employment_end_date,
                $value->manager ? $value->manager->name : '',
                $value->manager ? $value->manager->id: '',
            ]);
        }

        $this->count = $data->count();

        return $data;
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            BeforeSheet::class => function (BeforeSheet $event) {
                $event->sheet->getDelegate()->getSheetView()->setZoomScale(120);
            },
            AfterSheet::class => function (AfterSheet $event) {
                $bold = [
                    'font' => [
                        'bold' => true,
                    ],
                ];

                $font = [
                    'font' => [
                        'name' => 'Arial',
                    ],
                ];

                $header = [
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                        'startColor' => [
                            'argb' => 'FFB3B3B3',
                        ],
                        'endColor' => [
                            'argb' => 'FFB3B3B3',
                        ],
                    ],
                ];

                $cellRange = 'A1:A4'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray($bold);
                $event->sheet->getDelegate()->getStyle('A1:G' . $this->count)->applyFromArray($font);
                $event->sheet->getDelegate()->getStyle('A6:G6')->applyFromArray($header);
            },
        ];
    }
}
