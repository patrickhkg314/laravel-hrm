<?php

namespace App\Exports;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeSheet;

class HierarchyReport implements FromCollection, ShouldAutoSize, WithEvents
{
    protected $employee, $subordinates, $count;

    /**
     * HierarchyReport constructor.
     * @param $employee
     * @param $subordinates
     */
    public function __construct($employee, $subordinates)
    {
        $this->employee = $employee;
        $this->subordinates = $subordinates;
    }

    /**
     * @return array|\Illuminate\Support\Collection
     */
    public function collection()
    {
        $data = new Collection([
            ['Report Title', 'Hierarchy Report'],
            ['Blasting Date', Carbon::now()->toDateTimeString()],
            ['Staff ID', $this->employee->id],
            ['Name', $this->employee->name],
            [''],
            ['Direct Manager:']
        ]);

        $data->push(
            [
                [
                    'Manager Name:',
                    'Manager Staff ID:',
                ],
                [
                    $this->employee->manager == null ? '' : $this->employee->manager->name,
                    $this->employee->manager == null ? '' : $this->employee->manager->id,
                ],
                ['']
            ]
        );

        $data->push(
            [
                [
                    'Subordinates:',
                ],
                [
                    'Staff Name:',
                    'Staff ID:',
                    'Report Duty to Manager:',
                    'Manager ID:',
                ]
            ]
        );

        foreach ($this->subordinates as $key => $value) {
            $data->push(
                [
                    $value->name,
                    $value->id,
                    $value->manager == null ? '' : $value->manager->name,
                    $value->manager == null ? '' : $value->manager->id,
                ]
            );
        }

        $this->count = $data->count();

        return $data;
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            BeforeSheet::class => function (BeforeSheet $event) {
                $event->sheet->getDelegate()->getSheetView()->setZoomScale(200);
            },
            AfterSheet::class => function (AfterSheet $event) {
                $bold = [
                    'font' => [
                        'bold' => true,
                    ],
                ];

                $font = [
                    'font' => [
                        'name' => 'Arial',
                    ],
                ];

                $header = [
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                        'startColor' => [
                            'argb' => 'FFB3B3B3',
                        ],
                        'endColor' => [
                            'argb' => 'FFB3B3B3',
                        ],
                    ],
                ];

                $cellRange = 'A1:A4'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray($bold);
                $event->sheet->getDelegate()->getStyle('A1:D' . ($this->count + 3))->applyFromArray($font);
                $event->sheet->getDelegate()->getStyle('A6:D6')->applyFromArray($header);
                $event->sheet->getDelegate()->getStyle('A7:D7')->applyFromArray($header);
                $event->sheet->getDelegate()->getStyle('A10:D10')->applyFromArray($header);
                $event->sheet->getDelegate()->getStyle('A11:D11')->applyFromArray($header);
            },
        ];
    }
}
