<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Employee extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'employment_start_date'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
    ];

    /**
     * To check the manager
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function manager()
    {
        $now = Carbon::now();
        return $this->belongsTo('App\Models\Employee', 'manager_id')
            ->where('employment_start_date', '<=', $now->toDateString())
            ->where(function ($query) use ($now) {
                $query->where('employment_end_date', '>=', $now->toDateString())
                    ->orWhereNull('employment_end_date');
            });
    }

    /**
     * To check the subordinates (1 level only)
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subordinates()
    {
        $now = Carbon::now();
        return $this->hasMany('App\Models\Employee', 'manager_id')
            ->where('employment_start_date', '<=', $now->toDateString())
            ->where(function ($query) use ($now) {
                $query->where('employment_end_date', '>=', $now->toDateString())
                    ->orWhereNull('employment_end_date');
            });;
    }


}
