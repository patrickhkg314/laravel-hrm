<?php

namespace App\Exceptions;

use App\Services\ApiService;
use Exception;

class SelfDeleteException extends Exception
{
    protected $apiService;

    /**
     * SelfDeleteException constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->apiService = new ApiService();
    }

    /**
     * @return mixed
     */
    public function render()
    {
        return $this->apiService->returnFail(['self_deletion' => 'You cannot delete your account!']);
    }
}
