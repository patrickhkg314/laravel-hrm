<?php

namespace App\Exceptions;

use App\Services\ApiService;
use Exception;

class ValidatorException extends Exception
{
    protected $apiService, $errorArray;

    public function __construct(Array $errorArray)
    {
        parent::__construct();
        $this->apiService = new ApiService();
        $this->errorArray = $errorArray;
    }

    /**
     * @return mixed
     */
    public function render()
    {
        return $this->apiService->returnFail($this->errorArray);
    }
}
