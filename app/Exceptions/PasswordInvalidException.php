<?php

namespace App\Exceptions;

use App\Services\ApiService;
use Exception;

class PasswordInvalidException extends Exception
{
    protected $apiService;

    public function __construct()
    {
        parent::__construct();
        $this->apiService = new ApiService();
    }

    public function render()
    {
        return $this->apiService->returnFail(['old_password' => 'Old password is does not match.']);
    }
}
