<?php

namespace App\Repositories;

use App\Models\Employee;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class EmployeeRepository
{
    protected $employee;

    /**
     * EmployeeRepository constructor.
     * @param Employee $employee
     */
    public function __construct(Employee $employee)
    {
        $this->employee = $employee;
    }

    /**
     * @param $id
     * @param $password
     */
    public function updateEmployeePassword($id, $password)
    {
        $employee = $this->getEmployeeData($id);
        $employee->password = Hash::make($password);
        $employee->save();
    }

    /**
     * @param $id
     */
    public function deleteEmployee($id)
    {
        $employee = $this->getEmployeeData($id);
        $employee->delete();
    }

    /**
     * @return mixed
     */
    public function countAll()
    {
        return $this->employee->count();
    }

    /**
     * @param array $pagination
     * @return Employee[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAll(Array $pagination = [])
    {
        $dataSource = $this->employee->with(['manager', 'subordinates'])->get();
        $massagedData = clone($dataSource);

        // Sorting
        if (array_key_exists('sortBy', $pagination) && array_key_exists('descending', $pagination)) {
            $method = $pagination['descending'] ? 'sortByDesc' : 'sortBy';
            $massagedData = $dataSource->$method(function ($item) use ($pagination) {
                if ($pagination['sortBy'] == 'manager')
                    return $item->manager == null ? '' : $item->manager->name;
                else {
                    $sortBy = $pagination['sortBy'];
                    return $item->$sortBy;
                }
            })->values();
        }

        if (array_key_exists('rowsPerPage', $pagination) && array_key_exists('page', $pagination)) {
            if ($pagination['rowsPerPage'] != null && $pagination['page'] != null)
                $massagedData = $massagedData->forPage($pagination['page'], $pagination['rowsPerPage'])->values();
            else
                $massagedData = $massagedData->splice(0, 10);
        } else
            $massagedData = $massagedData->splice(0, 10);

        return $massagedData;
    }

    /**
     * @return Employee[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getActiveEmployeeData()
    {
        $now = Carbon::now();

        return $this->employee->with(['manager', 'subordinates'])
            ->where('employment_start_date', '<=', $now->endOfDay())
            ->where(function ($query) use ($now) {
                $query->where('employment_end_date', '>=', $now->startOfDay())
                    ->orWhereNull('employment_end_date');
            })->get();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getEmployeeData($id)
    {
        return $this->employee->with(['manager', 'subordinates'])->find($id);
    }

    /**
     * Get all subordinates of a single employee
     * Very careful for SQL injection and Data Leak!!!!
     *
     * @param $id
     * @return mixed
     */
    public function allSubordinates(Int $id)
    {
        $data = DB::select("select id,name,email,manager_id,employment_start_date,employment_end_date,created_at,updated_at from (select * from employees) employees, (select @pv := ?) initialisation where find_in_set(manager_id, @pv) > 0 and @pv := concat(@pv, ',', id)", [$id]);
        return $this->employee->hydrate($data);
    }

    /**
     * @param Employee $employee
     * @return bool
     */
    private function checkIsEmploying(Employee $employee)
    {
        $now = Carbon::now();

        $checkStart = Carbon::parse($employee->employment_start_date)->startOfDay()->lessThanOrEqualTo($now);

        if ($employee->employment_end_date != null)
            $checkEnd = Carbon::parse($employee->employment_end_date)->endOfDay()->greaterThanOrEqualTo($now);
        else
            $checkEnd = true;

        return $checkStart && $checkEnd;
    }

    /**
     * @param null $startDate
     * @param null $endDate
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getNewJoinerEmployeesListInPeriod($startDate = null, $endDate = null)
    {
        $data = $this->getAll();

        $filterData = $data->filter(function ($item) use ($startDate, $endDate) {
            if ($startDate == null && $endDate == null)
                return true;
            elseif ($startDate != null && $endDate == null)
                return Carbon::parse($item->employment_start_date)->greaterThanOrEqualTo(Carbon::parse($startDate)->startOfDay());
            elseif ($startDate == null && $endDate != null)
                return Carbon::parse($item->employment_start_date)->lessThanOrEqualTo(Carbon::parse($endDate)->endOfDay());
            else
                return Carbon::parse($item->employment_start_date)->isBetween(Carbon::parse($startDate)->startOfDay(),Carbon::parse($endDate)->endOfDay());
        })->values();

        return $filterData;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function checkEmployeeIndirectSubordinates($id)
    {
        $employee = $this->allSubordinates($id);

        $filteredEmployee = $employee->filter(function ($item) {
            return $this->checkIsEmploying($item);
        });

        $filteredEmployee->each(function ($item) {
            $item->manager; // Get relationship
        });

        return $filteredEmployee;
    }

    /**
     * @param array $dataArray
     * @param null $manager_id
     */
    public function addEmployee(Array $dataArray, $manager_id = null)
    {
        $employee = $this->employee->create($dataArray);

        if ($manager_id != null)
            $employee->manager()->associate($this->employee->find($manager_id))->save();
    }

    /**
     * @param array $dataArray
     * @param null $manager_id
     */
    public function editEmployee(Array $dataArray, $manager_id = null)
    {
        $employee = $this->employee->find($dataArray['id']);

        $employee->name = $dataArray['name'];
        $employee->email = $dataArray['email'];
        $employee->employment_start_date = $dataArray['employment_start_date'];
        $employee->employment_end_date = $dataArray['employment_end_date'];
        $employee->save();

        if ($manager_id != null)
            if ($manager_id != $employee->id)
                $employee->manager()->associate($this->employee->find($manager_id))->save();
            else
                $employee->manager()->dissociate()->save();
    }
}