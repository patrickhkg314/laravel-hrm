import Vue from 'vue'
import Router from 'vue-router'
import ProfilePage from '../views/ProfilePage'
import EmployeesPage from '../views/EmployeesPage'
import NewJoinerReport from '../views/NewJoinerReport'
import HierarchyReport from '../views/HierarchyReport'
import ReportPage from '../views/ReportPage'

Vue.use(Router);

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/hrm/employees',
            component: EmployeesPage
        },
        {
            path: '/hrm/profile',
            component: ProfilePage
        },
        {
            path: '/hrm/report',
            component: ReportPage
        },
        {
            path: '/hrm/report/new-joiner',
            component: NewJoinerReport
        },
        {
            path: '/hrm/report/hierarchy',
            component: HierarchyReport
        },
    ]
});