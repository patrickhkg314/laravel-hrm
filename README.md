# Tech Test
Demo: <http://patnet.network>

To build Locally, please follow the instruction.
# Before Installation
You need to install the following:
  - Vagrant     // You may skip it if you have local environment set up.
  - VirtualBox  // You may skip it if you not using Vagrant.
  - composer
  - PHP 7.1+
  - npm

# Installation
After git clone, run the following code:
```sh
$ composer install
$ npm install
$ npm run dev                               //You may use prod, dev, hot, watch
$ php vendor/bin/homestead make             //Skip it if you are not using vagrant
$ vagrant up                                //Skip it if you are not using vagrant
$ cp .env.example .env
$ vi .env                                   //You may need to change the setting. See # Setting Env
$ php artisan key:generate
$ vagrant ssh                               //Skip it if you are not using vagrant
$ php artisan migrate:refresh --seed        //You need to create the database first. See # Database Instruction
```

# Structure
```app``` folder contains all Controllers, Models, Services, Repositories and Middlewares.  
```routes``` folder contains all routings, but in this case, only ```routes/web.php``` is needed.  
```rosources``` folder contains all the web views (written in Vue.js).  
```databases``` folder contains all Migrations, Fakers and Seeders.  
  
```app\http\Controllers\HRM\*``` are the controllers used in the web views.  
```app\http\Serivces\*``` are the services injected and used by the controllers.  
```app\http\Repositories\*``` are the repositories injected and used by the services.  
```app\http\Models\*``` are the models used by repositories. Relationship of models also defined here.  
  
```resources\js\* ``` are the Vue.js files used in the web views.  
```resources\views\index.blade.php``` is the entry point of vue.js.  
```resources\views\auth\login.blade.php``` is the login page.  

# How to enter
Open a browser and type ``http://localhost:8000/`` if  you are using vagrant.  
P.S. You need use ```vagrant port ``` to check which port redirect to `80`port (Normally is ```8000```)  

# Database Instruction
Open any SQL tool to connecting MySQL port.  
Create a table ``homestead``                            // You can change it but remember to change `.env`  
Create a user ``homestead``                             // No need for vagrant. You can change it but remember to change `.env`  
Add/Change the password of user `homestead` to ``secret``    // No need for vagrant. You can change it but remember to change `.env`  

# Test
Gitlab CI has been setup, each commit will automatically test. You can download the coverage report from Gitlab. 

If you want run test locally, please add a database called ```hrm-test``` in order to work.
Then you can run ```vendor/bin/phpunit``` to run the test.

# Setting Env
It is suggested to change some .env setting for Vagrant. You may change the settings as your demand.  

P.S. You may use ```vagrant port ``` to check the port after run ```vagrant up```  

| Parameters | Suggested value |
| ------ | ------ |
| DB_USERNAME | 127.0.0.1 |
| DB_PORT | 33060 #check carefully |
| DB_DATABASE | homestead |
| DB_USERNAME | homestead |
| DB_PASSWORD | secret |