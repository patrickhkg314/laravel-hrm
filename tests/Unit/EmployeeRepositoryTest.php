<?php

namespace Tests\Unit;

use App\Models\Employee;
use App\Repositories\EmployeeRepository;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Hash;
use PDO;
use Tests\TestCase;

class EmployeeRepositoryTest extends TestCase
{
    use RefreshDatabase;

    protected $repository;

    public function setUp(): void
    {
        parent::setUp();
        $this->repository = new EmployeeRepository(new Employee());
    }

    public function testIfCanChangeEmployeePassword()
    {
        Artisan::call('migrate:refresh');

        $password = 'Test';
        $employeeFactory = factory(Employee::class)->create();


        $this->repository->updateEmployeePassword($employeeFactory->id, $password);

        $employee = $this->repository->getEmployeeData($employeeFactory->id);
        $this->assertEquals(true, Hash::check($password, $employee->password));
    }

    public function testDeleteEmployee()
    {
        Artisan::call('migrate:refresh');

        $employeeFactory = factory(Employee::class)->create();


        $this->repository->deleteEmployee($employeeFactory->id);

        $employee = Employee::all();
        $this->assertEquals(0, count($employee));
    }

    public function testCountAll()
    {
        Artisan::call('migrate:refresh');

        factory(Employee::class, 10)->create();



        $this->assertEquals(10, $this->repository->countAll());
    }

    public function testGetAll()
    {
        Artisan::call('migrate:refresh');

        factory(Employee::class, 5)->create();

        $pagination = [
            'sortBy' => 'id',
            'descending' => true,
            'rowsPerPage' => 10,
            'page' => 1,
        ];

        $this->assertEquals([5,4,3,2,1], $this->repository->getAll($pagination)->pluck('id')->toArray());

        $pagination = [
            'sortBy' => 'id',
            'descending' => false,
            'rowsPerPage' => 1,
            'page' => 1,
        ];

        $this->assertEquals([1], $this->repository->getAll($pagination)->pluck('id')->toArray());

        $this->assertEquals([1,2,3,4,5], $this->repository->getAll()->pluck('id')->toArray());

        Artisan::call('migrate:refresh');

        factory(Employee::class)->create([
            'name' => 'a',
            'employment_start_date' => Carbon::now()->subDays(3),
            'employment_end_date' => null,
        ]);

        factory(Employee::class)->create([
            'name' => 'b',
            'manager_id' => 1,
            'employment_start_date' => Carbon::now()->subDays(3),
            'employment_end_date' => null,
        ]);

        factory(Employee::class)->create([
            'name' => 'c',
            'manager_id' => 2,
            'employment_start_date' => Carbon::now()->subDays(3),
            'employment_end_date' => null,
        ]);

        factory(Employee::class)->create([
            'name' => 'd',
            'manager_id' => 3,
            'employment_start_date' => Carbon::now()->subDays(3),
            'employment_end_date' => null,
        ]);

        $pagination = [
            'sortBy' => 'manager',
            'descending' => true,
            'rowsPerPage' => 10,
            'page' => 1,
        ];

        $this->assertEquals(['c', 'b', 'a', null], $this->repository->getAll($pagination)->pluck('manager.name')->toArray());

        $pagination = [
            'sortBy' => 'manager',
            'descending' => false,
            'rowsPerPage' => 10,
            'page' => 1,
        ];

        $this->assertEquals([null, 'a', 'b', 'c'], $this->repository->getAll($pagination)->pluck('manager.name')->toArray());
    }

    public function testGetActiveEmployeeData() {
        Artisan::call('migrate:refresh');

        factory(Employee::class, 5)->create([
            'employment_start_date' => Carbon::now()->subDays(3),
            'employment_end_date' => Carbon::now()->subDays(3),
        ]);

        $this->assertEquals(0, count($this->repository->getActiveEmployeeData()));

        factory(Employee::class, 5)->create([
            'employment_start_date' => Carbon::now()->subDays(3),
            'employment_end_date' => Carbon::now(),
        ]);

        $this->assertEquals(5, count($this->repository->getActiveEmployeeData()));

        factory(Employee::class, 5)->create([
            'employment_start_date' => Carbon::now()->subDays(3),
            'employment_end_date' => null,
        ]);

        $this->assertEquals(10, count($this->repository->getActiveEmployeeData()));

        factory(Employee::class, 5)->create([
            'employment_start_date' => Carbon::now()->toDateString(),
            'employment_end_date' => null,
        ]);

        $this->assertEquals(15, count($this->repository->getActiveEmployeeData()));

        factory(Employee::class, 5)->create([
            'employment_start_date' => Carbon::now()->addDays(3),
            'employment_end_date' => null,
        ]);

        $this->assertEquals(15, count($this->repository->getActiveEmployeeData()));

        factory(Employee::class, 5)->create([
            'employment_start_date' => Carbon::now()->addDays(3),
            'employment_end_date' => Carbon::now(),
        ]);

        $this->assertEquals(15, count($this->repository->getActiveEmployeeData()));
    }

    public function testGetEmployeeData() {
        Artisan::call('migrate:refresh');

        $employee = factory(Employee::class)->create();


        $this->assertEquals($employee->name, $this->repository->getEmployeeData($employee->id)->name);
        $this->assertEquals($employee->email, $this->repository->getEmployeeData($employee->id)->email);
        $this->assertEquals($employee->employment_start_date, $this->repository->getEmployeeData($employee->id)->employment_start_date);
        $this->assertEquals($employee->employment_end_date, $this->repository->getEmployeeData($employee->id)->employment_end_date);
    }

    public function testAllSubordinates() {
        config([
            'database.default' => 'mysql',
            'database.connections.mysql' => [
                'driver' => 'mysql',
                'url' => env('DATABASE_URL'),
                'host' => env('DB_HOST', '127.0.0.1'),
                'port' => env('DB_PORT', '3306'),
                'database' => 'hrm-test',
                'username' => env('DB_USERNAME', 'forge'),
                'password' => env('DB_PASSWORD', ''),
                'unix_socket' => env('DB_SOCKET', ''),
                'charset' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'prefix' => '',
                'prefix_indexes' => true,
                'strict' => true,
                'engine' => null,
                'options' => extension_loaded('pdo_mysql') ? array_filter([
                    PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),
                ]) : [],
            ],
        ]);

        Artisan::call('migrate:refresh');

        factory(Employee::class)->create();
        factory(Employee::class,2)->create([
            'manager_id' => 1
        ]);
        factory(Employee::class,2)->create([
            'manager_id' => 2
        ]);
        factory(Employee::class,2)->create([
            'manager_id' => 3
        ]);


        $this->assertEquals(6, count($this->repository->allSubordinates(1)));
    }

    public function testGetNewJoinerEmployeesListInPeriod() {
        Artisan::call('migrate:refresh');

        $employee = factory(Employee::class, 5)->create([
            'employment_start_date' => Carbon::now(),
        ]);

        $startDate = null;
        $endDate = null;

        $this->assertEquals(5, count($this->repository->getNewJoinerEmployeesListInPeriod($startDate,$endDate)));

        $startDate = Carbon::now()->subDays(3);
        $endDate = Carbon::now()->subDays(1);

        $this->assertEquals(0, count($this->repository->getNewJoinerEmployeesListInPeriod($startDate,$endDate)));

        $startDate = Carbon::now()->subDays(3);
        $endDate = Carbon::now()->addDays(1);

        $this->assertEquals(5, count($this->repository->getNewJoinerEmployeesListInPeriod($startDate,$endDate)));

        $startDate = Carbon::now()->subDays(3);
        $endDate = null;

        $this->assertEquals(5, count($this->repository->getNewJoinerEmployeesListInPeriod($startDate,$endDate)));

        $startDate = Carbon::now()->addDays(3);
        $endDate = null;

        $this->assertEquals(0, count($this->repository->getNewJoinerEmployeesListInPeriod($startDate,$endDate)));

        $startDate = null;
        $endDate = Carbon::now()->subDays(3);

        $this->assertEquals(0, count($this->repository->getNewJoinerEmployeesListInPeriod($startDate, $endDate)));

        $startDate = Carbon::now()->addDays(3);
        $endDate = Carbon::now()->subDays(3);

        // Due to the function is Between, this also valid.
        $this->assertEquals(5, count($this->repository->getNewJoinerEmployeesListInPeriod($startDate,$endDate)));
    }

    public function testCheckEmployeeIndirectSubordinates() {
        config([
            'database.default' => 'mysql',
            'database.connections.mysql' => [
                'driver' => 'mysql',
                'url' => env('DATABASE_URL'),
                'host' => env('DB_HOST', '127.0.0.1'),
                'port' => env('DB_PORT', '3306'),
                'database' => 'hrm-test',
                'username' => env('DB_USERNAME', 'forge'),
                'password' => env('DB_PASSWORD', ''),
                'unix_socket' => env('DB_SOCKET', ''),
                'charset' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'prefix' => '',
                'prefix_indexes' => true,
                'strict' => true,
                'engine' => null,
                'options' => extension_loaded('pdo_mysql') ? array_filter([
                    PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),
                ]) : [],
            ],
        ]);

        Artisan::call('migrate:refresh');

        factory(Employee::class)->create();
        factory(Employee::class,2)->create([
            'employment_start_date' => Carbon::now()->addDays(3),
            'employment_end_date' => Carbon::now()->addDays(5),
            'manager_id' => 1
        ]);
        factory(Employee::class,2)->create([
            'employment_start_date' => Carbon::now()->subDays(3),
            'employment_end_date' => null,
            'manager_id' => 1
        ]);
        factory(Employee::class,2)->create([
            'employment_start_date' => Carbon::now()->subDays(3),
            'employment_end_date' => Carbon::now(),
            'manager_id' => 1
        ]);


        $this->assertEquals(4, count($this->repository->checkEmployeeIndirectSubordinates(1)));
    }

    public function testAddEmployee()
    {
        Artisan::call('migrate:refresh');

        $employeeData = factory(Employee::class)->make();

        $this->repository->addEmployee($employeeData->makeVisible(['password'])->toArray());

        $employee = Employee::all();
        $this->assertEquals(1, count($employee));
    }

    public function testAddEmployeeWithManager()
    {
        Artisan::call('migrate:refresh');

        factory(Employee::class)->create([
            'name' => 'Patrick',
            'employment_start_date' => Carbon::now()->subDays(3),
            'employment_end_date' => null
        ]);
        $employeeData = factory(Employee::class)->make();

        $this->repository->addEmployee($employeeData->makeVisible(['password'])->toArray(),1);

        $employee = Employee::all();
        $this->assertEquals(2, count($employee));
        $this->assertEquals('Patrick', Employee::with(['manager'])->find(2)->manager->name);
    }

    public function testEditEmployee()
    {
        Artisan::call('migrate:refresh');

        factory(Employee::class)->create([
            'name' => 'Patrick',
            'employment_start_date' => Carbon::now()->subDays(3),
            'employment_end_date' => null
        ]);
        $employeeData = factory(Employee::class)->make();

        $employeeData->id = 1;
        $this->repository->editEmployee($employeeData->toArray());

        $this->assertEquals($employeeData->name, Employee::find(1)->name);
        $this->assertEquals($employeeData->email, Employee::find(1)->email);
        $this->assertEquals($employeeData->employment_start_date, Employee::find(1)->employment_start_date);
    }

    public function testEditEmployeeWithManager()
    {
        Artisan::call('migrate:refresh');

        $manager = factory(Employee::class)->create([
            'name' => 'Patrick',
            'employment_start_date' => Carbon::now()->subDays(3),
            'employment_end_date' => null
        ]);
        factory(Employee::class)->create();
        $employeeData = factory(Employee::class)->make();

        $employeeData->id = 2;

        $this->repository->editEmployee($employeeData->toArray(),1);

        $this->assertEquals($employeeData->name, Employee::find(2)->name);
        $this->assertEquals($employeeData->email, Employee::find(2)->email);
        $this->assertEquals($employeeData->employment_start_date, Employee::find(2)->employment_start_date);
        $this->assertEquals($manager->name, Employee::with('manager')->find(2)->manager->name);
    }

    public function testEditEmployeeWithSelfManager()
    {
        Artisan::call('migrate:refresh');

        factory(Employee::class)->create();
        $employeeData = factory(Employee::class)->make();

        $employeeData->id = 1;

        $this->repository->editEmployee($employeeData->toArray(),1);

        $this->assertEquals($employeeData->name, Employee::find(1)->name);
        $this->assertEquals($employeeData->email, Employee::find(1)->email);
        $this->assertEquals($employeeData->employment_start_date, Employee::find(1)->employment_start_date);
        $this->assertEquals(null, Employee::with('manager')->find(1)->manager);
    }
}
