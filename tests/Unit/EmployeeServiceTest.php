<?php

namespace Tests\Unit;

use App\Exceptions\PasswordInvalidException;
use App\Exceptions\SelfDeleteException;
use App\Exceptions\ValidatorException;
use App\Models\Employee;
use App\Repositories\EmployeeRepository;
use App\Services\EmployeeService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class EmployeeServiceTest extends TestCase
{
    use RefreshDatabase;

    protected $mock;

    public function setUp(): void
    {
        parent::setUp();

        $this->mock = $this->getMockBuilder('App\Repositories\EmployeeRepository')
            ->disableOriginalConstructor()
            ->getMock();
    }

    public function testGetLoggedInEmployeeData()
    {
        Artisan::call('migrate:refresh');

        $employeeFactory = factory(Employee::class)->create();

        $this->be($employeeFactory);

        $service = new EmployeeService(new EmployeeRepository(new Employee()));
        $data = $service->getLoggedInEmployeeData();

        $this->assertEquals($employeeFactory->id, $data->id);
    }

    public function testGetActiveEmployees()
    {
        Artisan::call('migrate:refresh');

        $employeeFactory = factory(Employee::class, 12)->make();

        $this->mock->method('getActiveEmployeeData')->willReturn($employeeFactory);

        $service = new EmployeeService($this->mock);

        $this->assertEquals($employeeFactory, $service->getActiveEmployees());
    }

    public function testGetSingleEmployees()
    {
        Artisan::call('migrate:refresh');

        $employeeFactory = factory(Employee::class)->create();

        $this->mock->method('getEmployeeData')->willReturn($employeeFactory);
        $service = new EmployeeService($this->mock);

        $this->assertEquals($employeeFactory, $service->getSingleEmployees(1));
    }

    public function testGetSingleEmployeesException()
    {
        Artisan::call('migrate:refresh');

        $this->expectException(ValidatorException::class);

        $service = new EmployeeService($this->mock);
        $service->getSingleEmployees(1);
    }

    public function testGetAllEmployees()
    {
        Artisan::call('migrate:refresh');

        $employeeFactory = factory(Employee::class)->make();

        $this->mock->method('getAll')->willReturn($employeeFactory);
        $service = new EmployeeService($this->mock);

        $this->assertEquals($employeeFactory, $service->getAllEmployees([]));
    }

    public function testGetNewJoinerEmployeesListInPeriod()
    {
        Artisan::call('migrate:refresh');

        $employeeFactory = factory(Employee::class)->make();

        $this->mock->method('getNewJoinerEmployeesListInPeriod')->willReturn($employeeFactory);
        $service = new EmployeeService($this->mock);

        $this->assertEquals($employeeFactory, $service->getNewJoinerEmployeesListInPeriod([]));
    }

    public function testGetNewJoinerEmployeesListInPeriodValidationEndDateLessThanStartDate()
    {
        Artisan::call('migrate:refresh');

        $this->expectException(ValidatorException::class);

        $service = new EmployeeService($this->mock);

        $data = [
            'start_date' => '2019-01-01',
            'end_date' => '2018-01-01',
        ];

        $service->getNewJoinerEmployeesListInPeriod($data);
    }

    public function testGetNewJoinerEmployeesListInPeriodValidationInvalidDateFormat()
    {
        Artisan::call('migrate:refresh');

        $this->expectException(ValidatorException::class);

        $service = new EmployeeService($this->mock);

        $data = [
            'start_date' => '2019-99-01',
        ];

        $service->getNewJoinerEmployeesListInPeriod($data);
    }

    public function testExportNewJoinerEmployeesListInPeriodValidationEndDateLessThanStartDate()
    {
        Artisan::call('migrate:refresh');

        $this->expectException(ValidatorException::class);

        $service = new EmployeeService($this->mock);

        $data = [
            'start_date' => '2019-01-01',
            'end_date' => '2018-01-01',
        ];

        $service->exportNewJoinerEmployeesListInPeriod($data);
    }

    public function testExportNewJoinerEmployeesListInPeriodValidationInvalidDateFormat()
    {
        Artisan::call('migrate:refresh');

        $this->expectException(ValidatorException::class);

        $service = new EmployeeService($this->mock);

        $data = [
            'start_date' => '2019-99-01',
        ];

        $service->exportNewJoinerEmployeesListInPeriod($data);
    }

    public function testCountAllEmployee()
    {
        Artisan::call('migrate:refresh');

        $this->mock->method('countAll')->willReturn(12345);
        $service = new EmployeeService($this->mock);

        $this->assertEquals(12345, $service->countAllEmployees([]));
    }

    public function testChangeEmployeePassword()
    {
        Artisan::call('migrate:refresh');

        factory(Employee::class)->create([
            'password' => '$2y$10$xNdQsPR7eHwFuxa/3lSQTuqO75y5b50b4VnDyrXz8pzJVqu7/vqeu'
        ]);

        $service = new EmployeeService(new EmployeeRepository(new Employee()));
        $service->changeEmployeePassword([
            'old_password' => '12341234',
            'new_password' => '12345678',
            'new_password_confirmation' => '12345678',
        ], 1);

        $this->assertEquals(true, Hash::check('12345678', Employee::find(1)->password));
    }

    public function testChangeEmployeePasswordValidationWrongConfirmPassword()
    {
        Artisan::call('migrate:refresh');

        $this->expectException(ValidatorException::class);

        factory(Employee::class)->create([
            'password' => '$2y$10$xNdQsPR7eHwFuxa/3lSQTuqO75y5b50b4VnDyrXz8pzJVqu7/vqeu'
        ]);

        $service = new EmployeeService(new EmployeeRepository(new Employee()));
        $service->changeEmployeePassword([
            'old_password' => '12341234',
            'new_password' => '123',
            'new_password_confirmation' => '12345678',
        ], 1);
    }

    public function testChangeEmployeePasswordValidationOldPassword()
    {
        Artisan::call('migrate:refresh');

        $this->expectException(PasswordInvalidException::class);

        factory(Employee::class)->create([
            'password' => '$2y$10$xNdQsPR7eHwFuxa/3lSQTuqO75y5b50b4VnDyrXz8pzJVqu7/vqeu'
        ]);

        $service = new EmployeeService(new EmployeeRepository(new Employee()));
        $service->changeEmployeePassword([
            'old_password' => '123123',
            'new_password' => '12345678',
            'new_password_confirmation' => '12345678',
        ], 1);
    }

    public function testChangeEmployeePasswordValidationMissingField()
    {
        Artisan::call('migrate:refresh');

        $this->expectException(ValidatorException::class);

        $employee = factory(Employee::class)->create([
            'password' => '$2y$10$xNdQsPR7eHwFuxa/3lSQTuqO75y5b50b4VnDyrXz8pzJVqu7/vqeu'
        ]);

        $this->be($employee);

        $service = new EmployeeService(new EmployeeRepository(new Employee()));
        $service->changeEmployeePassword([
            'new_password' => '12345678',
            'new_password_confirmation' => '12345678',
        ]);
    }

    public function testCDeleteEmployee()
    {
        Artisan::call('migrate:refresh');

        $employee = factory(Employee::class)->create();

        $admin = factory(Employee::class)->create();
        $this->be($admin);

        $service = new EmployeeService(new EmployeeRepository(new Employee()));
        $service->deleteEmployee($employee->toArray());

        $this->assertEquals(1, count(Employee::all()));
    }

    public function testCDeleteEmployeeIfSameAccount()
    {
        Artisan::call('migrate:refresh');

        $this->expectException(SelfDeleteException::class);

        $employee = factory(Employee::class)->create();
        $this->be($employee);

        $service = new EmployeeService(new EmployeeRepository(new Employee()));
        $service->deleteEmployee($employee->toArray());
    }

    public function testAddEmployee()
    {
        Artisan::call('migrate:refresh');

        $employee = factory(Employee::class)->make()->toArray();
        $employee['password'] = '12341234';
        $employee['password_confirmation'] = '12341234';
        $employee['employment_end_date'] = null;

        $service = new EmployeeService(new EmployeeRepository(new Employee()));
        $service->addEmployee($employee);

        $this->assertEquals(1, count(Employee::all()));
    }

    public function testAddEmployeeValidationWrongConfirmPassword()
    {
        Artisan::call('migrate:refresh');

        $this->expectException(ValidatorException::class);

        $employee = factory(Employee::class)->make()->toArray();
        $employee['password'] = '12341234';
        $employee['password_confirmation'] = '123123';
        $employee['employment_end_date'] = null;

        $service = new EmployeeService(new EmployeeRepository(new Employee()));
        $service->addEmployee($employee);
    }

    public function testAddEmployeeValidationManagerNotExist()
    {
        Artisan::call('migrate:refresh');

        $this->expectException(ValidatorException::class);

        $employee = factory(Employee::class)->make()->toArray();
        $employee['password'] = '12341234';
        $employee['password_confirmation'] = '123123';
        $employee['employment_end_date'] = null;

        $service = new EmployeeService(new EmployeeRepository(new Employee()));
        $service->addEmployee($employee, 2);
    }

    public function testEditEmployee()
    {
        Artisan::call('migrate:refresh');

        $employee = factory(Employee::class)->create([
            'employment_start_date' => '2018-01-01',
            'employment_end_date' => '2019-01-01'
        ]);

        $data = [
            'id' => $employee->id,
            'email' => $employee->email,
            'name' => $employee->name,
            'employment_start_date' => '2017-01-01',
            'employment_end_date' => '2019-01-01',
        ];

        $service = new EmployeeService(new EmployeeRepository(new Employee()));
        $service->editEmployee($data);

        $this->assertEquals('2017-01-01', Employee::find(1)->employment_start_date);
    }

    public function testEditEmployeeValidationWrongConfirmPassword()
    {
        Artisan::call('migrate:refresh');

        $this->expectException(ValidatorException::class);

        $employee = factory(Employee::class)->make()->toArray();
        $employee['password'] = '12341234';
        $employee['password_confirmation'] = '1231234';
        $employee['employment_end_date'] = '1900-01-01';

        $service = new EmployeeService(new EmployeeRepository(new Employee()));
        $service->editEmployee($employee);
    }

    public function testEditEmployeeValidationManagerNotExist()
    {
        Artisan::call('migrate:refresh');

        $this->expectException(ValidatorException::class);

        $employee = factory(Employee::class)->make()->toArray();
        $employee['password'] = '12341234';
        $employee['password_confirmation'] = '123123';
        $employee['employment_end_date'] = null;

        $service = new EmployeeService(new EmployeeRepository(new Employee()));
        $service->editEmployee($employee, 2);
    }

    public function testCheckEmployeeIndirectSubordinates() {
        Artisan::call('migrate:refresh');

        $this->expectException(ValidatorException::class);

        $service = new EmployeeService(new EmployeeRepository(new Employee()));
        $service->checkEmployeeIndirectSubordinates(99);
    }
}
