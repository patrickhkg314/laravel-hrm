<?php

namespace Tests\Feature;

use App\Models\Employee;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Artisan;
use PDO;
use Tests\TestCase;

class EmployeesControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testIndex()
    {
        Artisan::call('migrate:refresh');

        $employee = factory(Employee::class)->create();

        $this->actingAs($employee)
            ->get('/employees')
            ->assertJson([
                'result_status' => 'Success',
                'result_data' => [
                    'data' => Employee::with(['manager', 'subordinates'])->get()->toArray(),
                    'total' => Employee::all()->count(),
                ]
            ]);
    }

    public function testGetActiveEmployees()
    {
        Artisan::call('migrate:refresh');

        $employee = factory(Employee::class)->create([
            'employment_start_date' => Carbon::now()->subDays(3),
            'employment_end_date' => null,
        ]);

        $now = Carbon::now();
        $data = Employee::with(['manager', 'subordinates'])
            ->where('employment_start_date', '<=', $now->endOfDay())
            ->where(function ($query) use ($now) {
                $query->where('employment_end_date', '>=', $now->startOfDay())
                    ->orWhereNull('employment_end_date');
            })->get();

        $this->actingAs($employee)
            ->get('/employees/active')
            ->assertJson([
                'result_status' => 'Success',
                'result_data' => $data->toArray(),
            ]);
    }

    public function testAdd()
    {
        Artisan::call('migrate:refresh');

        $admin = factory(Employee::class)->create();

        $employee = factory(Employee::class)->make()->toArray();
        $employee['password'] = '12341234';
        $employee['password_confirmation'] = '12341234';
        $employee['employment_end_date'] = null;

        $this->actingAs($admin)
            ->post('/employees', [
                'employee' => $employee,
                '_token' => csrf_token()
            ])
            ->assertJson([
                'result_status' => 'Success',
                'result_data' => [
                    'message' => 'OK'
                ]
            ]);
    }

    public function testAddValidation()
    {
        Artisan::call('migrate:refresh');

        $admin = factory(Employee::class)->create();

        $this->actingAs($admin)
            ->post('/employees')
            ->assertStatus(400);
    }

    public function testAddWithManager()
    {
        Artisan::call('migrate:refresh');

        $admin = factory(Employee::class)->create();

        $employee = factory(Employee::class)->make()->toArray();
        $employee['password'] = '12341234';
        $employee['password_confirmation'] = '12341234';
        $employee['employment_end_date'] = null;

        $this->actingAs($admin)
            ->post('/employees', [
                'employee' => $employee,
                'manager_id' => 999,
                '_token' => csrf_token()
            ])
            ->assertStatus(400);
    }

    public function testEdit()
    {
        Artisan::call('migrate:refresh');

        $admin = factory(Employee::class)->create();

        $employee = factory(Employee::class)->create([
            'password' => '12341234',
            'employment_end_date' => null,
        ]);

        $edit = $employee->toArray();
        $edit['employment_end_date'] = '2019-09-09';

        $this->actingAs($admin)
            ->put('/employees', [
                'employee' => $edit,
                '_token' => csrf_token()
            ]);

        $check = Employee::find($employee->id);

        $this->assertEquals('2019-09-09', $check->employment_end_date);
    }

    public function testEditValidation()
    {
        Artisan::call('migrate:refresh');

        $admin = factory(Employee::class)->create();

        $this->actingAs($admin)
            ->put('/employees')
            ->assertStatus(400);
    }

    public function testEditValidationWithManager()
    {
        Artisan::call('migrate:refresh');

        $admin = factory(Employee::class)->create();

        $employee = factory(Employee::class)->create([
            'password' => '12341234',
            'employment_end_date' => null,
        ]);

        $edit = $employee->toArray();
        $edit['employment_end_date'] = '2019-09-09';

        $this->actingAs($admin)
            ->put('/employees', [
                'employee' => $edit,
                'manager_id' => 82364782539,
                '_token' => csrf_token()
            ]);

        $this->actingAs($admin)
            ->post('/employees')
            ->assertStatus(400);
    }

    public function testDelete()
    {
        Artisan::call('migrate:refresh');

        $admin = factory(Employee::class)->create();

        $employee = factory(Employee::class)->create()->toArray();
        $employee['_token'] = csrf_token();

        $this->actingAs($admin)
            ->delete('/employees', $employee);

        $this->assertEquals(1, count(Employee::all()));
    }

    public function testDeleteValidation()
    {
        Artisan::call('migrate:refresh');

        $admin = factory(Employee::class)->create();

        $employee = factory(Employee::class)->create()->toArray();
        $employee['id'] = 1781293698;
        $employee['_token'] = csrf_token();

        $this->actingAs($admin)
            ->delete('/employees', $employee)
            ->assertStatus(400);
    }

    public function testDeleteOwnAccountValidation()
    {
        Artisan::call('migrate:refresh');

        $admin = factory(Employee::class)->create();

        $employee = $admin->toArray();

        $this->actingAs($admin)
            ->delete('/employees', $employee)
            ->assertStatus(400);
    }

    public function testGetNewJoinerEmployees()
    {
        Artisan::call('migrate:refresh');

        $admin = factory(Employee::class)->create();
        factory(Employee::class)->create([
            'manager_id' => 1,
            'employment_start_date' => '2017-08-09',
            'employment_end_date' => '2119-08-09',
        ]);

        Employee::with(['manager', 'subordinates'])->find(1);

        $expectOutput = [
            'result_status' => 'Success',
            'result_data' => [
                'data' => Employee::with(['manager', 'subordinates'])->get()->toArray(),
            ]
        ];

        $this->actingAs($admin)
            ->get('/employees/new-joiner')
            ->assertJson($expectOutput);
    }

    public function testExportNewJoinerEmployees()
    {
        Artisan::call('migrate:refresh');

        $admin = factory(Employee::class)->create();

        $this->actingAs($admin)
            ->get('/employees/new-joiner/export')
            ->assertOk();
    }

    public function testGetEmployeeHierarchy()
    {
        config([
            'database.default' => 'mysql',
            'database.connections.mysql' => [
                'driver' => 'mysql',
                'url' => env('DATABASE_URL'),
                'host' => env('DB_HOST', '127.0.0.1'),
                'port' => env('DB_PORT', '3306'),
                'database' => 'hrm-test',
                'username' => env('DB_USERNAME', 'forge'),
                'password' => env('DB_PASSWORD', ''),
                'unix_socket' => env('DB_SOCKET', ''),
                'charset' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'prefix' => '',
                'prefix_indexes' => true,
                'strict' => true,
                'engine' => null,
                'options' => extension_loaded('pdo_mysql') ? array_filter([
                    PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),
                ]) : [],
            ],
        ]);

        Artisan::call('migrate:refresh');

        factory(Employee::class)->create();
        $subordinates = factory(Employee::class)->create([
            'manager_id' => 1,
            'employment_start_date' => '2017-08-09',
            'employment_end_date' => '2119-08-09',
        ]);

        $employee = Employee::with(['manager', 'subordinates'])->find(1);

        $expectOutput = [
            'result_status' => 'Success',
            'result_data' => [
                'employee_data' => $employee->toArray(),
                'all_subordinates' => [
                    $subordinates->toArray()
                ],
            ]
        ];

        $this->actingAs($employee)
            ->get('/employees/hierarchy?id=1')
            ->assertJson($expectOutput);
    }


    public function testGetEmployeeHierarchyWithWrongId()
    {
        config([
            'database.default' => 'mysql',
            'database.connections.mysql' => [
                'driver' => 'mysql',
                'url' => env('DATABASE_URL'),
                'host' => env('DB_HOST', '127.0.0.1'),
                'port' => env('DB_PORT', '3306'),
                'database' => 'hrm-test',
                'username' => env('DB_USERNAME', 'forge'),
                'password' => env('DB_PASSWORD', ''),
                'unix_socket' => env('DB_SOCKET', ''),
                'charset' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'prefix' => '',
                'prefix_indexes' => true,
                'strict' => true,
                'engine' => null,
                'options' => extension_loaded('pdo_mysql') ? array_filter([
                    PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),
                ]) : [],
            ],
        ]);

        Artisan::call('migrate:refresh');

        factory(Employee::class)->create();
        $subordinates = factory(Employee::class)->create([
            'manager_id' => 1,
            'employment_start_date' => '2017-08-09',
            'employment_end_date' => '2119-08-09',
        ]);

        $employee = Employee::with(['manager', 'subordinates'])->find(1);

        $this->actingAs($employee)
            ->get('/employees/hierarchy?id=9')
            ->assertStatus(400);
    }

    public function testExportEmployeeHierarchy()
    {
        config([
            'database.default' => 'mysql',
            'database.connections.mysql' => [
                'driver' => 'mysql',
                'url' => env('DATABASE_URL'),
                'host' => env('DB_HOST', '127.0.0.1'),
                'port' => env('DB_PORT', '3306'),
                'database' => 'hrm-test',
                'username' => env('DB_USERNAME', 'forge'),
                'password' => env('DB_PASSWORD', ''),
                'unix_socket' => env('DB_SOCKET', ''),
                'charset' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'prefix' => '',
                'prefix_indexes' => true,
                'strict' => true,
                'engine' => null,
                'options' => extension_loaded('pdo_mysql') ? array_filter([
                    PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),
                ]) : [],
            ],
        ]);

        Artisan::call('migrate:refresh');

        $admin = factory(Employee::class)->create();

        factory(Employee::class)->create();
        factory(Employee::class)->create([
            'manager_id' => 1,
            'employment_start_date' => '2017-08-09',
            'employment_end_date' => '2119-08-09',
        ]);

        $this->actingAs($admin)
            ->get('/employees/hierarchy/export?id=' . $admin->id)
            ->assertOk();
    }
}
