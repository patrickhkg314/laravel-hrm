<?php

namespace Tests\Feature;

use App\Models\Employee;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Session;
use Tests\TestCase;

class LoginControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testLogin()
    {
        $this->withoutExceptionHandling();

        Artisan::call('migrate:refresh');

        $employee = factory(Employee::class)->create();

        Session::start();

        $this->followingRedirects()
            ->post('/login', [
                'email' => $employee->email,
                'password' => '12341234',
            ])->assertStatus(200);

        Session::flush();

        $this->post('/login', [
            'email' => $employee->email,
            'password' => '12341234',
        ])->assertRedirect('/hrm/home');
    }

    public function testFailedLogin()
    {
        Artisan::call('migrate:refresh');

        $this->post('/login', [
            'email' => '123',
            'password' => '12341234',
        ])
            ->assertRedirect('/');
    }

    public function testIfEmployeeDoNotLoginForAPage()
    {
        Artisan::call('migrate:refresh');

        $this->get('/hrm/xyz')
            ->assertRedirect('login');
    }

    public function testIfEmployeeDoNotLoginForAPageForJson()
    {
        Artisan::call('migrate:refresh');

        $this->get('/hrm/xyz', [
            'Accept' => 'application/json'
        ])->assertJson(['message' => "Unauthenticated."]);
    }

    public function testIfEmployeeLogout()
    {
        Artisan::call('migrate:refresh');

        $employee = factory(Employee::class)->create();

        $this->actingAs($employee)
            ->post('/logout',[
                '_token' => csrf_token()
            ])
            ->assertRedirect('/');
    }
}
