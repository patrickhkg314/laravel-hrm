<?php

namespace Tests\Feature;

use App\Models\Employee;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Artisan;
use PDO;
use Tests\TestCase;

class ProfileControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testGetLoggedInEmployeeDetail()
    {
        Artisan::call('migrate:refresh');

        $employee = factory(Employee::class)->create();

        $this->actingAs($employee)
            ->get('/logged-in-employee')
            ->assertJson([
                'result_status' => 'Success',
                'result_data' => $employee->toArray()
            ]);
    }

    public function testEmployeeChangePassword()
    {
        Artisan::call('migrate:refresh');

        $employee = factory(Employee::class)->create();

        $this->actingAs($employee)
            ->post('/password', [
                'old_password' => '12341234',
                'new_password' => '12345678',
                'new_password_confirmation' => '12345678',
                '_token' => csrf_token()
            ])
            ->assertJson([
                'result_status' => 'Success',
                'result_data' => [
                    'message' => 'OK'
                ]
            ]);
    }

    public function testEmployeeChangePasswordButWrongOldPassword()
    {
        Artisan::call('migrate:refresh');

        $employee = factory(Employee::class)->create();

        $this->actingAs($employee)
            ->post('/password', [
                'old_password' => '12341233',
                'new_password' => '12345678',
                'new_password_confirmation' => '12345678',
                '_token' => csrf_token()
            ])
            ->assertStatus(400);
    }

    public function testEmployeeChangePasswordWithMissingField()
    {
        Artisan::call('migrate:refresh');

        $employee = factory(Employee::class)->create();

        $expectOutput = [
            'result_status' => 'Fail',
            'error_message' => [
                'old_password' => ["The old password field is required."],
                'new_password' => ["The new password field is required."],
                'new_password_confirmation' => ["The new password confirmation field is required."]
            ]
        ];

        $this->actingAs($employee)
            ->post('/password')
            ->assertJson($expectOutput);
    }

    public function testEmployeeChangePasswordWithWrongConfirmPassword()
    {
        Artisan::call('migrate:refresh');

        $employee = factory(Employee::class)->create();

        $expectOutput = [
            'result_status' => 'Fail',
            'error_message' => [
                'new_password_confirmation' => ["The new password confirmation and new password must match."]
            ]
        ];

        $this->actingAs($employee)
            ->post('/password', [
                'old_password' => '12341234',
                'new_password' => '12345678',
                'new_password_confirmation' => '12345677',
                '_token' => csrf_token()
            ])
            ->assertJson($expectOutput);
    }

    public function testGetEmployeeManagerAndAllSubordinatesWithoutSubordinates() {
        config([
            'database.default' => 'mysql',
            'database.connections.mysql' => [
                'driver' => 'mysql',
                'url' => env('DATABASE_URL'),
                'host' => env('DB_HOST', '127.0.0.1'),
                'port' => env('DB_PORT', '3306'),
                'database' => 'hrm-test',
                'username' => env('DB_USERNAME', 'forge'),
                'password' => env('DB_PASSWORD', ''),
                'unix_socket' => env('DB_SOCKET', ''),
                'charset' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'prefix' => '',
                'prefix_indexes' => true,
                'strict' => true,
                'engine' => null,
                'options' => extension_loaded('pdo_mysql') ? array_filter([
                    PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),
                ]) : [],
            ],
        ]);

        Artisan::call('migrate:refresh');

        $employee = factory(Employee::class)->create();

        $expectOutput = [
            'result_status' => 'Success',
            'result_data' => [
                'employee_data' => $employee->toArray(),
                'all_subordinates' => [],
            ]
        ];

        $this->actingAs($employee)
            ->get('/employee-information')
            ->assertJson($expectOutput);
    }

    public function testGetEmployeeManagerAndAllSubordinatesWithSubordinates() {
        config([
            'database.default' => 'mysql',
            'database.connections.mysql' => [
                'driver' => 'mysql',
                'url' => env('DATABASE_URL'),
                'host' => env('DB_HOST', '127.0.0.1'),
                'port' => env('DB_PORT', '3306'),
                'database' => 'hrm-test',
                'username' => env('DB_USERNAME', 'forge'),
                'password' => env('DB_PASSWORD', ''),
                'unix_socket' => env('DB_SOCKET', ''),
                'charset' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'prefix' => '',
                'prefix_indexes' => true,
                'strict' => true,
                'engine' => null,
                'options' => extension_loaded('pdo_mysql') ? array_filter([
                    PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),
                ]) : [],
            ],
        ]);

        Artisan::call('migrate:refresh');

        factory(Employee::class)->create();
        $subordinates = factory(Employee::class)->create([
            'manager_id' => 1,
            'employment_start_date' => '2017-08-09',
            'employment_end_date' => '2119-08-09',
        ]);

        $employee = Employee::with(['manager','subordinates'])->find(1);

        $expectOutput = [
            'result_status' => 'Success',
            'result_data' => [
                'employee_data' => $employee->toArray(),
                'all_subordinates' => [
                    $subordinates->toArray()
                ],
            ]
        ];

        $this->actingAs($employee)
            ->get('/employee-information')
            ->assertJson($expectOutput);
    }
}
